package com.example.ankit.pronto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import java.util.Random;

public class ImageChanger extends AppCompatActivity implements Animation.AnimationListener {

    ImageView is;
    int[] images = {R.drawable.img1, R.drawable.img2, R.drawable.img3};
    int NUM_OF_IMAGES = 3;
    ScaleAnimation a1, a2, a3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_changer);
        startChanger();
    }

    protected void startChanger() {
        float delay;
        Intent intent = getIntent();
        delay = intent.getExtras().getFloat("delay");
        // Toast.makeText(getApplicationContext(),""+delay,Toast.LENGTH_LONG).show();
        is = (ImageView) findViewById(R.id.imageSwitcher);
//define your animations
        a1 = new ScaleAnimation(0.7f, 1.0f,1.0f, 1.0f);
    //    new ScaleAnimation()
        a2 = new ScaleAnimation(0.7f, 1.0f,1.0f, 1.0f);
        a3 = new ScaleAnimation(0.7f, 1.0f,1.0f, 1.0f);

// duration and behavior of ur animations
        a1.setFillAfter(true);
        a1.setDuration((long) (delay * 1000));
        a2.setFillAfter(true);
        a2.setDuration((long) (delay * 1000));
        a3.setFillAfter(true);
        a3.setDuration((long) (delay * 1000));

        a1.setAnimationListener(this);
        a2.setAnimationListener(this);
        a3.setAnimationListener(this);
        is.startAnimation(a1);
    }



    @Override
    public void onAnimationStart(Animation a) {
        if (a == a1) {
            is.setImageResource(images[0]);
        }
        if (a == a2) {
            is.setImageResource(images[1]);
        }
        if (a == a3) {
            is.setImageResource(images[2]);
        }
    }

    public void onAnimationEnd(Animation a) {
        if (a == a1) {
            if((new Random().nextInt(6))<2)
                is.startAnimation(a3);
            else
                is.startAnimation(a2);
        }

        if (a == a2) {
            if((new Random().nextInt(6))<2)
                is.startAnimation(a3);
            else
                is.startAnimation(a1);
        }

        if (a == a3) {
            if((new Random().nextInt(6))<3)
                is.startAnimation(a2);
            else
                is.startAnimation(a1);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
    // In the  case of using image switcher instead
//        is.setFactory(new ViewSwitcher.ViewFactory() {
//            @Override
//            public View makeView() {
//                ImageView myView = new ImageView(getApplicationContext());
//                myView.setScaleType(ImageView.ScaleType.FIT_CENTER);
//                myView.setLayoutParams(new
//                        ImageSwitcher.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
//                        ActionBar.LayoutParams.WRAP_CONTENT));
//                return myView;
//            }
//        });
//        Animation in = AnimationUtils.loadAnimation(this,android.R.anim.slide_in_left);
//        Animation out = AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right);
//        is.setInAnimation(in);
//        is.setOutAnimation(out);
//        Handler handler = new Handler();
//        int j = 0;
//        while(j<4)
//        {
//            for(final int i: images)
//            {
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Log.d("Filter", "Value of i: " + i);
//                        is.setImageResource(i);
//                    }
//                }, 2500);
//            }
//            j++;
//        }
}


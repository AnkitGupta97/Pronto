package com.example.ankit.pronto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class GetTime extends AppCompatActivity {
    Button start;
    EditText etime;
    float time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_time);
        start = (Button) findViewById(R.id.bstart);
        start.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etime = (EditText) findViewById(R.id.etime);
                if(!etime.getText().toString().isEmpty())
                {
                    time= Float.valueOf(etime.getText().toString());
                    startexercise();
                }
            }

        });
    }
    public void startexercise() {
        final Thread myThread = new Thread() {
            public void run() {
                Intent intent = new Intent(getApplicationContext(), ImageChanger.class);
                intent.putExtra("delay", time);
                startActivity(intent);
            }};
        myThread.start();
    }

}
